<!-- une variable globale qui contient le post courant -->
<?php global $post ?>
<div>
    <label for="rating">Rating :</label>
    <input id="rating" type="number" name="rating" min="0" max="10"
        value="<?php echo get_post_meta($post->ID, 'rating', true) ?>">
</div>
<div>
    <label for="release_date">Release Date: </label>
    <input id="release_date" type="date" name="release_date"
        value="<?php echo get_post_meta($post->ID, 'release_date', true) ?>">
</div>

    <div class="col-3">
        <a href="<?php the_permalink() ?>">
        <div class="card">
            <div class="card-body">

                <?php if(get_post_thumbnail_id()){ ?>
                    <?php the_post_thumbnail('post-thumbnail', [
                        'class' => 'card-img-top'
                    ]) ?>
                <?php } else { ?>
                    <img class="card-img-top"
                            src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/placeholder.png">
                <?php } ?>

                <div class="summary">
                    <?php the_excerpt() ?>
                </div>
                
            </div>
            <div class="card-footer">
                <?php the_title() ?>
            </div>
        </div>
        </a>
    </div>
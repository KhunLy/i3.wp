<?php the_post() ?>
<div class="my-3">
<?php if(get_the_post_thumbnail_url()){ ?>
    <?php the_post_thumbnail(null, [
        'class' => 'post-img',
    ]) ?>
<?php } else { ?>
    <img class="post-img" src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/placeholder.png">
<?php } ?>
<h2 class="text-dark"><?php the_title() ?></h2>
<h4 class="text-muted">
    <?php the_date() ?> by 
    <a href="<?php the_author_url() ?>"><?php the_author() ?></a>
</h4>
<?php the_content() ?>
<!-- <?php foreach(get_the_tags() as $item){ ?>
    <span><?php var_dump($item) ?>,</span>
<?php } ?> -->
<?php if(get_the_tags()){ ?>
    <?php echo implode('', array_map(function($item) {
        return sprintf('<a class="tag" href="%s">%s</a>',
            get_tag_link($item->term_id),
            $item->name
        );
    }, get_the_tags())) ?>
<?php } ?>
<?php if(comments_open()){ ?>
    <?php comments_template() ?>
<?php } ?>
</div>
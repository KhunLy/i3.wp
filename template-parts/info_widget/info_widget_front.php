<ul class="info">
    <li><i class="fa fa-envelope"></i> <?php echo $email ?></li>
    <li><i class="fa fa-user"></i> <?php echo $address ?></li>
    <li><i class="fa fa-phone"></i> <?php echo $tel ?></li>
</ul>

<style>
.info {
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;
}

.info>li {
    margin-top:10px;
    margin-bottom:10px;
}
</style>
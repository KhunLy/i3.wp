<?php

/* hook wordpress cfr: addEventListener en js */
add_action('after_setup_theme', function() {
    /* ajoute une fonctionnalité au theme */
    add_theme_support('custom-logo', [
        'height' => 150,
        'width' => 150
    ]);

    add_theme_support('custom-header');

    add_theme_support('post-thumbnails');

    // enregistrer des emplacements de menus
    register_nav_menus([
        "primary_menu" => "Primary",
        "socials_links_menu" => "Socials Links"
    ]);

    // ajouter un nouveau type de "post"
    register_post_type('games', [
        // affichage dans l'ui
        'labels' => [
            'name' => 'Games',
            'add_new' => 'Add new Game'
        ],
        'public' => true,
        // fonctionalités de base à ajouter
        'supports' => [
            'thumbnail',
            'title',
            'excerpt',
            'author',
            'editor'
        ],
        'show_in_rest' => true
    ]);

    // ajouter une taxonomie à un type de post
    register_taxonomy('platforms', 'games', [
        'labels' => [
            'name' => 'Platforms'
        ],
        'show_in_rest' => true
    ]);

    register_taxonomy('dev', 'games', [
        'labels' => [
            'name' => 'Developer'
        ],
        'show_in_rest' => true
    ]);

    register_taxonomy('pub', 'games', [
        'labels' => [
            'name' => 'Publisher'
        ],
        'show_in_rest' => true
    ]);

    register_taxonomy('categories', 'games', [
        'labels' => [
            'name' => 'Categories'
        ],
        'show_in_rest' => true
    ]);
});

//ajouter une nouvelle "box" dans un écran
add_action('add_meta_boxes', function() {
    add_meta_box(
        'game_meta',
        // titre de la box 
        'Game Meta Datas', 
        function(){
            // contenu de la box
            get_template_part('template-parts/admin/game_fields');
        },
        // l'écran dans lequel on souhaite ajouter la "box" 
        'games'
    );
});

// sauvegarde des metas informations
add_action('save_post', function($id, $post, $isUpading){
    $metas = [];
    if(isset($_POST['release_date'])){
        $metas['release_date'] = $_POST['release_date'];
    }
    if(
        isset($_POST['rating']) 
        && $_POST['rating'] >= 0 
        && $_POST['rating'] <= 10
        ){
        $metas['rating'] = $_POST['rating'];
    }
    foreach($metas as $k => $v){
        if($isUpading){
            update_post_meta($id, $k, $v);
        }
        else{
            add_post_meta($id,$k, $v, true);
        }
    }
}, 10, 3);

add_action('wp_enqueue_scripts', function(){
    // fct wp pui permet de register un style
    wp_register_style('myStyle', get_stylesheet_directory_uri().'/style.css');
    wp_enqueue_style('myStyle');
    if(is_front_page()){
        //fct qui permet de register un script
        wp_register_script('lastGames', 
            get_stylesheet_directory_uri().'/assets/js/lastGames.js',
            [], false, true
        );
        wp_enqueue_script('lastGames');
    }
});

// add_filter('wp_nav_menu_items', function($items, $args){
//     // if($args->theme_location === 'socials_links_menu'){
//     //     preg_match('(?:[a-z][a-z]+)(<\/a>)', $items, $matches, PREG_OFFSET_CAPTURE);
//     // }
//     return $items;
// }, 10, 2);

// add_filter('the_title', function($content) {
//     return '♥' . strtoupper($content) . '♥';
// });

add_filter('comment_text', function($content) {
    $motsACensures = [
        "merde",
        "saperlipopette",
    ];
    foreach($motsACensures as $mot){
        $content 
            = str_replace($mot, starify($mot), $content);
    }
    $content = str_replace(':)', '😀' ,$content);
    return $content;
});

function starify($mot){
    return implode('', 
        array_map(function(){ return '*'; },
        str_split($mot)
        )
    );
}


require_once __DIR__.'/widgets/info_widget/info_widget.php';
add_action('widgets_init', function() {
    register_widget('info_widget');
});

register_sidebar(['name' => 'footer_sidebar']);

add_shortcode('logo', function() {
    return get_custom_logo();
});


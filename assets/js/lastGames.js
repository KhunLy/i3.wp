let url = 'http://localhost/wp_custom/wp-json/wp/v2/games?per_page=2';
let template = `
<div>
    <a href="__url__">
        <img src="__img__">
    </a>
</div>`;

let loader = '<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';

$('#loader').html(loader);
$.get(url, function(data, status, req) {
    // recuperer le nombre total de posts, jeux, pages, ...
    console.log(req.getResponseHeader('X-WP-Total'));
    for(let item of data){
        const title = item['title']['rendered'];
        const link = item['link'];
        const imgEP = item['_links']['wp:featuredmedia'][0]['href'];
        $.get(imgEP, function(data) {
            const img = data['media_details']['sizes']['thumbnail']['source_url'];

            $('#results').append(
                template.replace('__url__', link)
                .replace('__img__', img)
            );
            $('#loader').empty();
        });
    }
});

// $.ajax({
//     url: url,
//     method: 'GET',
//     dataType: 'JSON',
//     success: function() {

//     },
//     error: function() {

//     },
//     complete: function() {

//     }
// });
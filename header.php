<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); wp_title('-') ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <?php wp_head() ?>
</head>
<body>

    <header class="" style="color: #<?php echo get_header_textcolor() ?>;background-image: url('<?php echo get_header_image() ?>')">
        <!-- <?php the_custom_header_markup() ?> -->
        
        <div class="mx-2">
            <?php the_custom_logo() ?>
        </div>
        <div class="mx-2">
            <h1><?php bloginfo('name') ?></h1>
            <h2><?php bloginfo('description') ?></h2>
        </div>
        <nav>
            <?php wp_nav_menu([
                'theme_location' => 'primary_menu',
                'menu_id' => 'menu-main',
            ]) ?>
        </nav>
    </header>
<?php  
class Info_Widget extends WP_Widget {

    public function __construct(){
        //info du widget
        parent::__construct(
            //id du widget
            'info_widget', 
            //nom du widget
            'Info Widget', [
                //description du widget
                'description' => 'Infos du site'
            ]
        );
    }

    // afficher le widget
    public function widget( $args, $instance ){
        $email = $instance['email'] ?: 'adresse@bidon.com';
        $address = $instance['address'] ?: '';
        $tel = $instance['tel'] ?: '';
        include __DIR__.'/../../template-parts/info_widget/info_widget_front.php';
    }

    //mettre à jour le widget
    public function update( $new_instance, $old_instance ) {
        $instance = [];
        $instance['email'] = $new_instance['email'];
        $instance['tel'] = $new_instance['tel'];
        $instance['address'] = $new_instance['address'];
        return $instance;
    }

    //afficher le formulaire du widget
    public function form( $instance ) { 
        include __DIR__.'/../../template-parts/info_widget/info_widget_back.php';
    }
}
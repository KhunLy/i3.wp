<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); wp_title('-') ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <?php wp_head() ?>
</head>
<body>

    <header class="front-header">
        <div class="mx-2">
            <h1><?php bloginfo('name') ?></h1>
            <h2><?php bloginfo('description') ?></h2>
        </div>
        <nav>
            <?php wp_nav_menu([
                'theme_location' => 'primary_menu',
                'menu_id' => 'menu-main',
            ]) ?>
        </nav>
    </header>
    <?php query_posts('posts_per_page=5'); $count = 0; ?>
    <div class="container-fluid">
        <div class="img-container row">
            <?php while(have_posts() && $count < 5) { ?>
            <?php if($count == 0 || $count == 1 || $count == 4){ ?><div class="column col-md-4"><?php } ?>
                <div class="front-img">
                    <div>
                        <div style="background-image:url('<?php the_post_thumbnail_url() ?>')"></div>
                    </div>
                </div>
                <?php if($count == 0 || $count == 3 || $count == 4 ){ ?></div><?php } ?>
                <?php $count++; ?>

            <?php } ?>
        </div>
    </div>
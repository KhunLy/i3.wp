<?php get_header(); 
    $cats = get_the_terms(get_the_ID(), 'categories');
    $pubs = get_the_terms(get_the_ID(), 'pub');
    $devs = get_the_terms(get_the_ID(), 'dev');
    $platforms = get_the_terms(get_the_ID(), 'platforms');
    the_post();
?>
<!-- récupérer les termes d'un post particulier -->
<!-- <?php get_the_terms(get_the_ID(), 'platforms') ?> -->

<!-- récupérer les meta data d'un post -->
<!-- <?php get_post_meta(get_the_ID(), 'release_date', true) ?> -->

<div class="container my-3">
    <h1><?php the_title() ?></h1>
    <p><?php echo get_post_meta(get_the_ID(), 'release_date', true) ?></p>
    <?php the_post_thumbnail('large') ?>
    <div class="star-ratings-css">
    <div class="star-ratings-css-top" style="width: 
    
    <?php echo get_post_meta(get_the_ID(), 'rating', true) ?>0%">
    <span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
    <div class="star-ratings-css-bottom"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>
    </div>

    <div><?php echo get_the_content() ?></div>

    <p>Categories: <?php echo implode(', ', array_map(function($item){
        return $item->name;
    },$cats ? $cats : [])); ?></p>
    <p>Platforms: <?php echo implode(', ', array_map(function($item){
        return $item->name;
    },$platforms ? $platforms : [])); ?></p>
    <p>Developer: <?php echo implode(', ', array_map(function($item){
        return $item->name;
    },$devs ? $devs : [])); ?></p>
    <p>Publisher: <?php echo implode(', ', array_map(function($item){
        return $item->name;
    },$pubs ? $pubs : [])); ?></p>
</div>

<?php get_footer() ?>
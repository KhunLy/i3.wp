<footer class="container-fluid">
        <div class="row">
            <div class="col">
            <?php dynamic_sidebar('footer_sidebar') ?>
            </div>
            <div class="col">
            <?php wp_nav_menu([
                'theme_location' => 'socials_links_menu',
                'menu_id' => 'menu-footer'
            ]) ?>
            </div>
        </div>
        <div class="text-light bg-dark row" id="footer-bottom">&copy;BStorm - 2019</div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <?php wp_footer() ?>
</body>
</html>
<?php get_header() ?>
<div class="container my-3">
    <h1><i class="fa fa-cog"></i>PAGE NOT FOUND</h1>

    <h2>Suggestions:</h2>
    <?php 
        $req = explode('/',$_SERVER['REQUEST_URI']);
        $keyword = $req[count($req) - 1];
    ?>

    <?php $query = new WP_Query([
        's' => $keyword
    ]) ?>
    <div class="row">
    <?php while($query->have_posts()) { 
        $query->the_post();
        get_template_part('template-parts/post/content');
    } ?>
    </div>
</div>
<?php get_footer() ?>
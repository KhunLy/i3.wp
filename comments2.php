<!-- have_comments retourne false si aucun commentaire n'a été posté -->
<?php if(have_comments()){ ?>
    <!-- récupérer le nombre de commentaires -->
    <!-- <?php echo get_comments_number() ?> -->
    <!-- afficher la liste des commentaires -->
    <?php  ?>
    <?php wp_list_comments([
        'avatar_size' => 100,
    ]) ?>
<?php } else { ?>
    <p>No Comments yet</p>
<?php } ?>
<!-- afficher le formulaire pour poster un commentaire -->
<?php comment_form([
    'class_submit' => 'btn btn-info',
    'class_form' => 'form-horizontal',
    'comment_field' => '
        <label for="content" class="control-label">Comment :</label>
        <div class="form-group">
            <textarea id="content" class="form-control" name="content" rows="10"></textarea>
        </div>
    '
]) ?>